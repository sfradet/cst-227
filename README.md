# **CST-227**
## Shawn Fradet
## August 10 2020 - September 27 2020

---
## **About**
My name is Shawn Fradet and this is a repository for my CST-227 class at Grand Canyon University.


## **Contributors**
Name | Email
-----|-------
Shawn Fradet | <Sfradet@my.gcu.edu>