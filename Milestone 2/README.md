# **CST-227 MineSweeper Project**
## Shawn Fradet
## August 10 2020 - September 27 2020

---
## **About**
This is a C# based version of MineSweeper for CST-227 at Grand Canyon University.

## **Milestone 2**
### **Requirements**
* Make the game playable
### **Files**
#### **\Milestone 1 Directory**
\MineSweeperMilestone2 				\- Contains the C# files for running the game  
\UML Diagram 			\- UML Diagram for the project  
\Flow Chart			\- Flow chart of game operation

### **Running the code**
The current version of the project is a C# console application. Download the project files and load
the project into Visual Studio. Run the project.


## **Contributors**
Name | Email
-----|-------
Shawn Fradet | <Sfradet@my.gcu.edu>