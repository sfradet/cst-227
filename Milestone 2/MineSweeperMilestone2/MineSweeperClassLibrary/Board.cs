﻿/*
 * Minesweeper Milestone 2
 * Shawn Fradet
 * CST-227
 * 8/21/2020
 *
 * This is my own work.
 *
 * This is the Board file for a minesweeper console game.
 */

using System;

namespace MineSweeperClassLibrary
{
    public class Board
    {
        public int Size { get; set; } // Size of the board, Size x Size
        public Cell[,] Grid { get; set; } // Array of Cells for board
        public double Difficulty { get; set; } = .10; // Difficultly of game as percentage

        // Board constructor. Takes size as an argument
        public Board(int size)
        {
            this.Size = size;
            this.Grid = new Cell[Size, Size]; // Set Cell array to Size x Size

            // Step through Cell array and create new Cell in each index
            for (int row = 0; row < Size; row++)
            {
                for (int col = 0; col < Size; col++)
                {
                    Grid[row, col] = new Cell();
                    Grid[row, col].Row = row;
                    Grid[row, col].Col = col;
                }
            }
        }

        // Method for planting bombs in the Board
        public void SetupLiveNeighbors()
        {
            int cells = Size * Size; // Find number of cells in board

            // Multiply number of cells by difficultly percentage and round up for bomb count
            int bombs = (int)Math.Ceiling(cells * Difficulty);
            // Create new random number generator
            var rand = new Random();

            int count = 0; // Counter for bombs

            // Randomly generate (x,y) coordinates and place bomb on board if not already a bomb.
            while (count < bombs)
            {
                int randRow = rand.Next(Size);
                int randCol = rand.Next(Size);
                if (Grid[randRow, randCol].Live != true)
                {
                    Grid[randRow, randCol].Live = true;
                    count++;
                }
            }
        }

        // Find the a Cell's number of neighbors that are bombs.
        public void CalculateLiveNeighbors()
        {
            // Step through each Cell in Grid and check valid neighbors for bombs.
            for (int row = 0; row < Size; row++)
            {
                for (int col = 0; col < Size; col++)
                {
                    if (IsValid(row - 1, col - 1) && Grid[row - 1, col - 1].Live)
                    {
                        Grid[row, col].LiveNeighbors++;
                    }
                    if (IsValid(row - 1, col) && Grid[row - 1, col].Live)
                    {
                        Grid[row, col].LiveNeighbors++;
                    }
                    if (IsValid(row - 1, col + 1) && Grid[row - 1, col + 1].Live)
                    {
                        Grid[row, col].LiveNeighbors++;
                    }
                    if (IsValid(row, col - 1) && Grid[row, col - 1].Live)
                    {
                        Grid[row, col].LiveNeighbors++;
                    }
                    if (IsValid(row, col + 1) && Grid[row, col + 1].Live)
                    {
                        Grid[row, col].LiveNeighbors++;
                    }
                    if (IsValid(row + 1, col - 1) && Grid[row + 1, col - 1].Live)
                    {
                        Grid[row, col].LiveNeighbors++;
                    }
                    if (IsValid(row + 1, col) && Grid[row + 1, col].Live)
                    {
                        Grid[row, col].LiveNeighbors++;
                    }
                    if (IsValid(row + 1, col + 1) && Grid[row + 1, col + 1].Live)
                    {
                        Grid[row, col].LiveNeighbors++;
                    }
                }
            }
        }

        // Method that checks if a Grid location is valid
        public bool IsValid(int row, int col)
        {
            // Check if cell is within the array
            if (row >= 0 && row < Size && col >= 0 && col < Size)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}