﻿/*
 * Minesweeper Milestone 3
 * Shawn Fradet
 * CST-227
 * 8/30/2020
 *
 * This is my own work.
 *
 * This is the program file that starts and loops through MineSweeper game play.
 */

using MineSweeperClassLibrary;
using System;

namespace MineSweeperMilestone3
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Board board = new Board(12); // Create a board that is 12x12
            board.SetupLiveNeighbors(); // Populate board with bombs
            board.CalculateLiveNeighbors(); // Determine cells live neighbors
            bool endGame = false; // Boolean to represent end of game
            PrintBoardDuringGame(board, endGame); // Print the board

            while (!endGame)
            {
                // Request row from user.
                Console.Write("Please enter a row: ");
                int row;
                while (!int.TryParse(Console.ReadLine(), out row) || row < 0 || row >= board.Size)
                {
                    Console.WriteLine("Please enter a valid row number");
                }

                // Request column from user
                Console.Write("Please enter a column: ");
                int col;
                while (!int.TryParse(Console.ReadLine(), out col) || col < 0 || col >= board.Size)
                {
                    Console.WriteLine("Please enter a valid row column");
                }

                // Check if visited already. If not set to visited and redraw the board.
                if (board.Grid[row, col].Visited)
                {
                    Console.WriteLine("Already Visited");
                }
                else
                {
                    board.FloodFill(row, col);
                    Console.Clear();
                    PrintBoardDuringGame(board, endGame);
                }

                bool allVisited = true; // Boolean to check if all tiles are visited
                for (int i = 0; i < board.Size; i++)
                {
                    for (int j = 0; j < board.Size; j++)
                    {
                        // Step through all tiles. If tile is not a bomb and not visited, allVisited is false.
                        if (!board.Grid[i, j].Visited && !board.Grid[i, j].Live)
                        {
                            allVisited = false;
                        }
                    }
                }

                // If player chose bomb tile, you lose. If all non-bomb tiles visited, you win.
                if (board.Grid[row, col].Live)
                {
                    Console.WriteLine("You Lose");
                    endGame = true;
                }
                else if (allVisited)
                {
                    Console.WriteLine("You Win");
                    endGame = true;
                }
            }

            PrintBoardDuringGame(board, endGame); // Print board with bombs exposed
            Console.WriteLine("Game Over, Press enter to exit");
            Console.ReadLine();
        }

        // Method to print the board to the console. Takes Board object as argument
        public static void PrintBoard(Board board)
        {
            // Print top row of board
            Console.Write("+");
            for (int i = 0; i < board.Size; i++)
            {
                if (i < 10)
                {
                    Console.Write(" " + i + " +");
                }
                else
                {
                    Console.Write(" " + i + "+");
                }
            }
            Console.Write("\n");

            // Print board Cells
            int row = 0; // Row counter
            while (row < board.Size)
            {
                // Print row separator
                Console.Write("+");
                for (int i = 0; i < board.Size; i++)
                {
                    Console.Write("---+");
                }
                Console.Write("\n");
                // Print row of Cells
                Console.Write("|");
                for (int col = 0; col < board.Size; col++)
                {
                    if (board.Grid[row, col].Live) // If a bomb print *
                    {
                        Console.Write(" * |");
                    }
                    else // If not a bomb, print live neighbor quantity
                    {
                        Console.Write(" " + board.Grid[row, col].LiveNeighbors + " |");
                    }
                }
                Console.Write(" " + row + "\n");
                row++;
            }

            // Print bottom row of board
            Console.Write("+");
            for (int i = 0; i < board.Size; i++)
            {
                Console.Write("---+");
            }
            Console.WriteLine();
        }

        // Method to print current board status during a game
        public static void PrintBoardDuringGame(Board board, bool endGame)
        {
            // Print top row of board
            Console.WriteLine();
            Console.Write("+");
            for (int i = 0; i < board.Size; i++)
            {
                if (i < 10)
                {
                    Console.Write(" " + i + " +");
                }
                else
                {
                    Console.Write(" " + i + "+");
                }
            }
            Console.Write("\n");

            // Step through an print board cells
            int count = 0; // Counter
            while (count < board.Size)
            {
                // Print row separator line
                Console.Write("+");
                for (int i = 0; i < board.Size; i++)
                {
                    Console.Write("---+");
                }
                Console.Write("\n");

                // Print row
                Console.Write("|");
                for (int i = 0; i < board.Size; i++)
                {
                    // If cell has no live neighbors, print blank space.
                    if (board.Grid[count, i].Visited && board.Grid[count, i].LiveNeighbors == 0 && !board.Grid[count, i].Live)
                    {
                        Console.Write("   |");
                    }
                    // If player selects bomb, print *. If game is over, print all bombs
                    else if ((board.Grid[count, i].Visited && board.Grid[count, i].Live) || (board.Grid[count, i].Live && endGame))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(" * ");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("|");
                    }
                    // Print live neighbors for visited cell
                    else if (board.Grid[count, i].Visited)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write(" " + board.Grid[count, i].LiveNeighbors);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write(" |");
                    }
                    // Non-visited cell prints '?'
                    else
                    {
                        Console.Write(" ? |");
                    }
                }
                Console.Write(" " + count + "\n");
                count++;
            }
            // Print bottom row
            Console.Write("+");
            for (int i = 0; i < board.Size; i++)
            {
                Console.Write("---+");
            }
            Console.WriteLine();
        }
    }
}