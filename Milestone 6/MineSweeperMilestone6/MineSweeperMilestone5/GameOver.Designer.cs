﻿namespace MineSweeperMilestone5
{
    partial class GameOver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblResult = new System.Windows.Forms.Label();
            this.textTime = new System.Windows.Forms.TextBox();
            this.txtInitials = new System.Windows.Forms.TextBox();
            this.lblInitals = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblResult
            // 
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResult.Location = new System.Drawing.Point(42, 27);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(247, 65);
            this.lblResult.TabIndex = 0;
            this.lblResult.Text = "YOU LOST!";
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textTime
            // 
            this.textTime.BackColor = System.Drawing.Color.Black;
            this.textTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTime.ForeColor = System.Drawing.Color.White;
            this.textTime.Location = new System.Drawing.Point(116, 104);
            this.textTime.Name = "textTime";
            this.textTime.ReadOnly = true;
            this.textTime.Size = new System.Drawing.Size(90, 35);
            this.textTime.TabIndex = 3;
            this.textTime.TabStop = false;
            this.textTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtInitials
            // 
            this.txtInitials.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInitials.Location = new System.Drawing.Point(166, 164);
            this.txtInitials.MaxLength = 3;
            this.txtInitials.Name = "txtInitials";
            this.txtInitials.Size = new System.Drawing.Size(48, 24);
            this.txtInitials.TabIndex = 1;
            this.txtInitials.Text = "AAA";
            this.txtInitials.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtInitials.Visible = false;
            // 
            // lblInitals
            // 
            this.lblInitals.AutoSize = true;
            this.lblInitals.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInitals.Location = new System.Drawing.Point(99, 167);
            this.lblInitals.Name = "lblInitals";
            this.lblInitals.Size = new System.Drawing.Size(61, 18);
            this.lblInitals.TabIndex = 5;
            this.lblInitals.Text = "Initials:";
            this.lblInitals.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(116, 215);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(90, 35);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // GameOver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 262);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblInitals);
            this.Controls.Add(this.txtInitials);
            this.Controls.Add(this.textTime);
            this.Controls.Add(this.lblResult);
            this.Name = "GameOver";
            this.Text = "Game Over";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.TextBox textTime;
        private System.Windows.Forms.TextBox txtInitials;
        private System.Windows.Forms.Label lblInitals;
        private System.Windows.Forms.Button btnOk;
    }
}