﻿/*
 * Minesweeper Milestone 5
 * Shawn Fradet
 * CST-227
 * 9/20/2020
 *
 * This is my own work.
 *
 * This form displays winning or losing message.
 */

using MineSweeperClassLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace MineSweeperMilestone5
{
    public partial class GameOver : Form
    {
        private bool winner = false; // Was the user a winner
        private PlayerStats player; // Holds player stats

        // Path to save file
        private string pathString = System.IO.Path.Combine(Application.StartupPath, "scores.txt");

        // Constructor for losing game
        public GameOver(TimeSpan time)
        {
            InitializeComponent();
            this.CenterToParent();

            // Populate timer
            textTime.Text = time.ToString(@"mm\.ss");
        }

        // Constructor for winning game
        public GameOver(bool winner, PlayerStats player)
        {
            InitializeComponent();
            this.CenterToParent();

            // Set winner and player
            this.winner = winner;
            this.player = player;

            // Set form controls
            if (winner)
            {
                lblResult.Text = "YOU WON!";
                textTime.Text = player.time.ToString(@"mm\.ss");
                lblInitals.Visible = true;
                txtInitials.Visible = true;
            }

            saveScores(); // Update high scores
        }

        // OK button event handler
        private void btnOk_Click(object sender, EventArgs e)
        {
            // If player won game
            if (winner)
            {
                player.PlayerInitials = txtInitials.Text; // Update player initials
                HighScoreList.highScoreList.Add(player); // Add player to high scores
                saveScores(); // Save high scores
            }

            this.Close();
        }

        public void saveScores()
        {
            // Create list of strings to hold high score information
            List<string> outputLines = new List<string>();

            // Step through high scores list and add information to string list
            foreach (PlayerStats p in HighScoreList.highScoreList)
            {
                outputLines.Add(p.ToString());
            }

            // Write to file
            File.WriteAllLines(pathString, outputLines);
        }
    }
}