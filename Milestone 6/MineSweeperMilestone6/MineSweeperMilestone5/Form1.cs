﻿/*
 * Minesweeper Milestone 5
 * Shawn Fradet
 * CST-227
 * 9/13/2020
 *
 * This is my own work.
 *
 * This is the opening menu for Minesweeper.
 */

using MineSweeperClassLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MineSweeperMilestone5
{
    public partial class Form1 : Form
    {
        // High score list file location
        private string pathString = System.IO.Path.Combine(Application.StartupPath, "scores.txt");

        public Form1()
        {
            InitializeComponent();
            this.CenterToParent();

            loadScores(); // Load the high scores from file
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            int size = 8;
            int difficulty = 1;

            // Check radio buttons for difficulty and board size
            if (rdoEasy.Checked) { difficulty = 1; }
            if (rdoNormal.Checked) { difficulty = 2; }
            if (rdoHard.Checked) { difficulty = 3; }
            if (rdoSmall.Checked) { size = 8; };
            if (rdoMedium.Checked) { size = 12; };
            if (rdoLarge.Checked) { size = 16; };

            // Open game
            GameBoard gameBoard = new GameBoard(size, difficulty);
            gameBoard.ShowDialog();
        }

        // Load high scores from file
        public void loadScores()
        {
            List<String> lines = new List<String>();

            // Check if file exists. If file, read all lines. If no file, create one.
            if (File.Exists(pathString))
            {
                lines = File.ReadAllLines(pathString).ToList();
            }
            else
            {
                File.Create(pathString);
            }

            // Step through list and add files to high score list
            foreach (string line in lines)
            {
                string[] entries = line.Split(',');
                HighScoreList.highScoreList.Add(new PlayerStats(entries[0], TimeSpan.Parse(entries[1]), double.Parse(entries[3]), int.Parse(entries[2])));
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}