﻿/*
 * Minesweeper Milestone 6
 * Shawn Fradet
 * CST-227
 * 9/20/2020
 *
 * This is my own work.
 *
 * This class represents a form with high scores
 */

using MineSweeperClassLibrary;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace MineSweeperMilestone5
{
    public partial class HighScores : Form
    {
        private int size; // Size of the board
        private double difficulty; // Difficulty level

        public HighScores(int boardSize, double difficulty)
        {
            InitializeComponent();
            this.CenterToParent();

            // Set variables
            this.size = boardSize;
            this.difficulty = difficulty;

            // Populate high score list
            getTopFive();
        }

        public void getTopFive()
        {
            // LINQ Query that returns top five scores by board size and difficulty
            var topFive = (
                from player in HighScoreList.highScoreList
                where player.BoardSize == this.size && player.DifficultyLevel == this.difficulty
                orderby player
                select player).Take(5);

            int place = 1; // High score place
            int y = 50; // Y location coordinate

            foreach (var player in topFive)
            {
                // Create label for initials and time
                Label lblInitials = new Label();
                Label lblTime = new Label();
                lblInitials.AutoSize = true;
                lblTime.AutoSize = true;

                // Populate label text
                lblInitials.Text = place + ".  " + player.PlayerInitials;
                lblTime.Text = player.time.ToString(@"mm\.ss");

                // Place labels by point and add to group box
                lblInitials.Location = new Point(20, y);
                lblTime.Location = new Point(120, y);
                grpEasy.Controls.Add(lblInitials);
                grpEasy.Controls.Add(lblTime);

                // Advance variables
                y += 40;
                place++;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}