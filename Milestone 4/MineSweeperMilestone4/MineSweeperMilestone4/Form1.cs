﻿/*
 * Shawn Fradet
 * CST-227
 * Milestone 4
 *
 * Form that gets difficulty level
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MineSweeperMilestone4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.CenterToParent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            // Create a gameBoard Form and display
            GameBoard gameBoard = new GameBoard();
            gameBoard.ShowDialog();
            this.Close();
        }
    }
}
