﻿/*
 * Shawn Fradet
 * CST-227
 * Milestone 4
 *
 * Form that displays panel of buttons. Buttons change color and
 * give click counter information when clicked.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MineSweeperMilestone4
{
    public partial class GameBoard : Form
    {
        public static int size = 10; // Size of the board
        public static int buttonClicks = 0; // Button click counter
        public Button[,] btnArr = new Button[size, size]; // Array of buttons

        public GameBoard()
        {
            InitializeComponent();
            this.CenterToParent();
            PopulateBoard(); // Populates the board when loaded
        }

        // Method to populate the game board
        public void PopulateBoard()
        {
            // Set the button size to the width divided by the amount of buttons
            int buttonSize = pnlGame.Width / 10;

            // Step through each row and column of button array and create new button
            for (int r = 0; r < size; r++)
            {
                for (int c = 0; c < size; c++)
                {
                    // Create new button and set size and width
                    btnArr[r, c] = new Button();
                    btnArr[r, c].Width = buttonSize;
                    btnArr[r, c].Height = buttonSize;

                    // Create click event for button
                    btnArr[r, c].Click += Grid_Button_Click;

                    // Add button to panel and assign location
                    pnlGame.Controls.Add(btnArr[r, c]);
                    btnArr[r, c].Location = new Point(buttonSize * r, buttonSize * c);

                    // Set tag to row and column information
                    btnArr[r, c].Tag = r.ToString() + "|" + c.ToString();
                }
            }
        }

        // Event handler for button click
        private void Grid_Button_Click(object sender, EventArgs e)
        {
            buttonClicks++; // Increment counter

            // Get row and column from Tag value and set to integers
            string[] strArr = (sender as Button).Tag.ToString().Split('|');
            int r = int.Parse(strArr[0]);
            int c = int.Parse(strArr[1]);

            // Create random color
            Random rand = new Random();
            int red = rand.Next(256);
            int green = rand.Next(256);
            int blue = rand.Next(256);
            Color color = Color.FromArgb(red, green, blue);

            // Change clicked button background color
            btnArr[r, c].BackColor = color;

            // Set button text to click counter
            btnArr[r, c].Font = new Font("Arial", 10, FontStyle.Bold);
            btnArr[r, c].Text = buttonClicks.ToString();
        }
    }
}