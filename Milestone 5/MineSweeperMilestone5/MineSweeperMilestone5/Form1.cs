﻿/*
 * Minesweeper Milestone 5
 * Shawn Fradet
 * CST-227
 * 9/13/2020
 *
 * This is my own work.
 *
 * This is the opening menu for Minesweeper.
 */
using System;
using System.Windows.Forms;

namespace MineSweeperMilestone5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.CenterToParent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            int size = 8;
            int difficulty = 1;

            // Check radio buttons for difficulty and board size
            if (rdoEasy.Checked) { difficulty = 1; }
            if (rdoNormal.Checked) { difficulty = 2; }
            if (rdoHard.Checked) { difficulty = 3; }
            if (rdoSmall.Checked) { size = 8; };
            if (rdoMedium.Checked) { size = 12; };
            if (rdoLarge.Checked) { size = 16; };

            // Open game
            GameBoard gameBoard = new GameBoard(size, difficulty);
            gameBoard.ShowDialog();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}