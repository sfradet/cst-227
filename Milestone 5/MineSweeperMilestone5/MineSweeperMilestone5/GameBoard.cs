﻿/*
 * Minesweeper Milestone 5
 * Shawn Fradet
 * CST-227
 * 9/13/2020
 *
 * This is my own work.
 *
 * This is the Main Program file for a minesweeper WinForm game.
 */

using MineSweeperClassLibrary;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace MineSweeperMilestone5
{
    public partial class GameBoard : Form
    {
        private Board board; // Instance of game board
        public static int size; // Size of the board
        public Button[,] btnArr; // Array of buttons
        public Stopwatch stopwatch = new Stopwatch(); // Stopwatch
        public TimeSpan time = new TimeSpan(); // Timespan for formatting time
        public const int ButtonSize = 34; // Size of the panel buttons

        public GameBoard(int boardSize, int difficulty)
        {
            InitializeComponent();

            // Setup game board dimensions
            size = boardSize;
            board = new Board(boardSize, difficulty);
            btnArr = new Button[size, size];

            // Resize the form to fit game board
            this.Width = (size * 34) + 90;
            this.Height = this.Width + 50;
            this.CenterToParent();

            board.SetupLiveNeighbors(); // Populate board with bombs
            board.CalculateLiveNeighbors(); // Determine cells live neighbors
            PopulateBoard(); // Populates the board when loaded
        }

        // Method to populate the game board
        public void PopulateBoard()
        {
            // Setup button panel size
            pnlBoard.Width = (size * 34) + 1;
            pnlBoard.Height = pnlBoard.Width;

            // Position button and timer relative to button panel
            textBox1.Location = new Point(pnlBoard.Location.X, 15);
            btnQuit.Location = new Point(pnlBoard.Location.X + pnlBoard.Width - btnQuit.Width, 15);

            // Step through each row and column of button array and create new button
            for (int r = 0; r < size; r++)
            {
                for (int c = 0; c < size; c++)
                {
                    // Create new button and set height and width
                    btnArr[r, c] = new Button();
                    btnArr[r, c].Width = ButtonSize;
                    btnArr[r, c].Height = ButtonSize;

                    // Create click event for button
                    btnArr[r, c].Click += Grid_Button_Click;
                    btnArr[r, c].MouseDown += Grid_Button_MouseDown;

                    // Add button to panel and assign location
                    pnlBoard.Controls.Add(btnArr[r, c]);
                    btnArr[r, c].Location = new Point(ButtonSize * r, ButtonSize * c);

                    // Set tag to row and column information
                    btnArr[r, c].Tag = r.ToString() + "|" + c.ToString();
                }
            }
        }

        // Event handler for left button click
        private void Grid_Button_Click(object sender, EventArgs e)
        {
            // Get row and column from Tag value and set to integers
            string[] strArr = (sender as Button).Tag.ToString().Split('|');
            int r = int.Parse(strArr[0]);
            int c = int.Parse(strArr[1]);

            // Start the turn
            takeTurn(r, c);
        }

        // Event for right button click
        private void Grid_Button_MouseDown(object sender, MouseEventArgs e)
        {
            // Get button grid location
            string[] strArr = (sender as Button).Tag.ToString().Split('|');
            int r = int.Parse(strArr[0]);
            int c = int.Parse(strArr[1]);

            // Check if right button click
            if (e.Button == MouseButtons.Right)
            {
                // If no image loaded, load flag image. If image loaded, set to null.
                if (btnArr[r, c].Image == null)
                {
                    btnArr[r, c].Image = imageList1.Images[2];
                    btnArr[r, c].Image.Tag = "Flag";
                }
                else
                {
                    btnArr[r, c].Image = null;
                }
            }
        }

        // Method that handles one turn event
        private void takeTurn(int r, int c)
        {
            stopwatch.Start();

            // Check if clicked button is bomb or marked with flag
            if (!board.Grid[r, c].Live && btnArr[r, c].Image == null)
            {
                board.FloodFill(r, c); // Call flood fill to open board spaces

                // Step through all cells in the board
                for (int row = 0; row < board.Size; row++)
                {
                    for (int col = 0; col < board.Size; col++)
                    {
                        // If cell is not a bomb and has no live neighbors, remove the button
                        if (board.Grid[row, col].Visited && board.Grid[row, col].LiveNeighbors == 0 && !board.Grid[row, col].Live)
                        {
                            pnlBoard.Controls.Remove(btnArr[row, col]);
                        }
                        // If cell is a bomb and has been clicked, set to bomb image
                        else if ((board.Grid[row, col].Visited && board.Grid[row, col].Live) || (board.Grid[row, col].Live))
                        {
                            btnArr[r, c].Image = imageList1.Images[0];
                        }
                        // If cell has been visited remove button and display live neighbors
                        else if (board.Grid[row, col].Visited)
                        {
                            pnlBoard.Controls.Remove(btnArr[row, col]); // Remove the button

                            // Create a label, assign it number of live neighbors, place it on panel
                            Label NeighborCount = new Label();
                            NeighborCount.Text = board.Grid[row, col].LiveNeighbors.ToString();
                            NeighborCount.Location = new Point((ButtonSize * row + (ButtonSize / 3)), (ButtonSize * col + (ButtonSize / 3)));
                            NeighborCount.Font = new Font("Arial", 10, FontStyle.Bold);
                            NeighborCount.ForeColor = getColor(board.Grid[row, col].LiveNeighbors);
                            NeighborCount.AutoSize = true;
                            pnlBoard.Controls.Add(NeighborCount);
                        }
                    }
                }
            }

            // Check if all cells have been visited
            bool endgame = allVisited();

            // If game over, change live cells to display bomb or flag
            if ((board.Grid[r, c].Live && btnArr[r, c].Image == null) || endgame)
            {
                for (int row = 0; row < board.Size; row++)
                {
                    for (int col = 0; col < board.Size; col++)
                    {
                        if (board.Grid[r, c].Live && board.Grid[row, col].Live)
                        {
                            btnArr[row, col].Image = imageList1.Images[0];
                        }
                        else if (endgame && board.Grid[row, col].Live)
                        {
                            btnArr[row, col].Image = imageList1.Images[2];
                        }
                    }
                }
            }

            // Display applicable end game message
            if (board.Grid[r, c].Live && (string)btnArr[r, c].Image.Tag != "Flag")
            {
                stopwatch.Stop();
                MessageBox.Show("YOU LOSE!\nElapsed time: " + textBox1.Text, "Game Over");
                this.Close();
            }
            else if (endgame)
            {
                stopwatch.Stop();
                MessageBox.Show("YOU WIN!\nElapsed time: " + textBox1.Text, "Winner");
                this.Close();
            }
        }

        // Method for checking if all cells have been visited
        public bool allVisited()
        {
            bool allVisited = true; // Boolean to check if all tiles are visited
            for (int row = 0; row < board.Size; row++)
            {
                for (int col = 0; col < board.Size; col++)
                {
                    // Step through all tiles. If tile is not a bomb and not visited, allVisited is false.
                    if (!board.Grid[row, col].Visited && !board.Grid[row, col].Live)
                    {
                        allVisited = false;
                    }
                }
            }
            return allVisited;
        }

        // Stopwatch control
        private void timer1_Tick(object sender, EventArgs e)
        {
            time = stopwatch.Elapsed;
            textBox1.Text = time.ToString(@"mm\.ss");
        }

        // Quit button click method
        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // Method for painting grid lines on the panel
        private void pnlBoard_Paint(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;

            // Paint horizontal lines
            for (int x = 0; x <= pnlBoard.Width; x += 34)
            {
                var p = new Pen(Color.LightGray, 1);
                var point1 = new Point(0, x);
                var point2 = new Point(pnlBoard.Width, x);
                g.DrawLine(p, point1, point2);
            }
            // Paint vertical lines
            for (int x = 0; x <= pnlBoard.Width; x += 34)
            {
                var p = new Pen(Color.LightGray, 1);
                var point1 = new Point(x, 0);
                var point2 = new Point(x, pnlBoard.Width);
                g.DrawLine(p, point1, point2);
            }
        }

        // Method for returning color for panel labels
        public Color getColor(int neighbors)
        {
            switch (neighbors)
            {
                case 1:
                    return Color.Blue;

                case 2:
                    return Color.Green;

                case 3:
                    return Color.Red;

                case 4:
                    return Color.Purple;

                case 5:
                    return Color.Maroon;

                case 6:
                    return Color.Turquoise;

                case 7:
                    return Color.Black;

                case 8:
                    return Color.Gray;

                default:
                    return Color.Blue; ;
            }
        }
    }
}