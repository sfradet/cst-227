# **CST-227 MineSweeper Project**
## Shawn Fradet
## August 10 2020 - September 27 2020

---
## **About**
This is a C# based version of MineSweeper for CST-227 at Grand Canyon University.

## **Milestone 5**
### **Requirements**
* Combine console game logic with WinForms presentation to create Minesweeper game 
### **Files**
#### **\Milestone 5 Directory**
\MineSweeperMilestone5 				\- Contains the C# files for running the game  


### **Running the code**
The current version of the project is a C# WinForm application. Download the project files and load
the project into Visual Studio. Run the project.


## **Contributors**
Name | Email
-----|-------
Shawn Fradet | <Sfradet@my.gcu.edu>