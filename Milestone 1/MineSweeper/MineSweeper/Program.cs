﻿/*
 * Minesweeper Milestone 1
 * Shawn Fradet
 * CST-227
 * 8/15/2020
 *
 * This is my own work.
 *
 * This is the Program file for a minesweeper console game.
 */

using System;

namespace MineSweeper
{
    public class Program
    {
        private static void Main(string[] args)
        {
            Board board = new Board(12); // Create a board that is 12x12
            board.SetupLiveNeighbors(); // Populate board with bombs
            board.CalculateLiveNeighbors(); // Determine cells live neighbors
            PrintBoard(board); // Display the board
        }

        // Method to print the board to the console. Takes Board object as argument
        public static void PrintBoard(Board board)
        {
            // Print top row of board
            Console.Write("+");
            for (int i = 0; i < board.Size; i++)
            {
                if (i < 10)
                {
                    Console.Write(" " + i + " +");
                }
                else
                {
                    Console.Write(" " + i + "+");
                }
            }
            Console.Write("\n");

            // Print board Cells
            int row = 0; // Row counter
            while (row < board.Size)
            {
                // Print row separator
                Console.Write("+");
                for (int i = 0; i < board.Size; i++)
                {
                    Console.Write("---+");
                }
                Console.Write("\n");
                // Print row of Cells
                Console.Write("|");
                for (int col = 0; col < board.Size; col++)
                {
                    if (board.Grid[row, col].Live) // If a bomb print *
                    {
                        Console.Write(" * |");
                    }
                    else // If not a bomb, print live neighbor quantity
                    {
                        Console.Write(" " + board.Grid[row, col].LiveNeighbors + " |");
                    }
                }
                Console.Write(" " + row + "\n");
                row++;
            }

            // Print bottom row of board
            Console.Write("+");
            for (int i = 0; i < board.Size; i++)
            {
                Console.Write("---+");
            }
            Console.WriteLine();
        }
    }
}